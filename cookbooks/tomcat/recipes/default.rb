#
# Cookbook:: tomcat
# Recipe:: default
#
# Copyright:: 2020, The Authors, All Rights Reserved.

include_recipe 'java'

directory '/opt/tomcat' do
  owner 'root'
  group 'root'
  mode '0755'
  action :create
end

user 'tomcat' do
  comment 'tomcat'
  home '/opt/tomcat'
  system true
  shell '/bin/false'
 end

group 'tomcat' do
  action :create
end


